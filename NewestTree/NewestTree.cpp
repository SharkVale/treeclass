﻿// NewestTree.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "TreeClass.h"
using namespace std;

int main()
{
	int count1 = 0;
	Tree Tr;
	char exit = 0;
	cout << "Enter elements of the first tree\n";
	while (exit != 'q')	
	{
		int val = 0;
		cout << "Enter a number " << count1+1 <<": ";
		cin >> val;
		count1++;
		cout << "\n";
		Tr.add(val, 0);
		cout << "If you want to exit, press 'q' : ";
		cin >> exit;
		cout << "\n";
	}
	cout << "Tree N1:\n";
	Tr.showarr();
	cout << "Direct travel:\n";
	Tr.directTravel(0);
	cout << "Symmetry travel:\n";
	Tr.symmetryTravel(0);
	////////////////////////////////////
	count1 = 0;
	Tree Tr2;
	char exit2 = 0;
	cout << "\nInput elements of the second tree\n";
	while (exit2 != 'q')
	{
		int val = 0;
		cout << "Input a number " << count1 + 1 << ": ";
		cin >> val;
		count1++;
		cout << "\n";
		Tr2.add(val, 0);
		cout << "If you want to exit, press 'q' : ";
		cin >> exit2;
		cout << "\n";
	}
	cout << "Tree N1:\n";
	Tr2.showarr();
	cout << "Direct travel:\n";
	Tr2.directTravel(0);
	cout << "\nSymmetry travel:\n";
	Tr2.symmetryTravel(0);
	
	///////////////////////////////////////
	Tr.intersection(Tr2);
}

